%define debug_package %{nil}

Name:           kdigger
Version:        1.5.0
Release:        1%{?dist}
Summary:        Kubernetes focused container assessment and context discovery tool for penetration testing

License:        ASL 2.0
URL:            https://github.com/quarkslab/kdigger
Source0:        https://github.com/quarkslab/%{name}/releases/download/v%{version}/%{name}-linux-amd64.tar.gz

%description
Kubernetes focused container assessment and 
context discovery tool for penetration testing

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name}-linux-amd64 %{buildroot}/usr/bin/kdigger

%files
/usr/bin/kdigger

%changelog
* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.5.0
* Sun Oct 16 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.4.0
* Thu Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
